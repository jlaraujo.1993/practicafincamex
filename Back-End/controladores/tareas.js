'use strict'

const Tareas = require('../modelos/tareas');
const moment = require('moment');

//Guardar Tarea
function Guardar(req, res) {
    let tareas = new Tareas();
    tareas.Titulo = req.body.Titulo;
    tareas.Responsable = req.body.Responsable;
    tareas.FechaInicio = req.body.FechaInicio;
    tareas.FechaFin = req.body.FechaFin;
    tareas.PorcentajeAvance = req.body.PorcentajeAvance;
    tareas.Categoria = req.body.Categoria;
    tareas.ListadoSubTareas = req.body.ListadoSubTareas;
    tareas.save((err, tareasStored) => {
        if (err) {
            Respuesta(res, 500, 'Error al guardar en la base de datos.' + err, null);
        }
        Respuesta(res, 200, 'Tarea registrada con éxito.', tareasStored);
    });
}


//Consultar Tareas
function Consultar(req, res) {
    Tareas.find({}, (err, tareasStored) => {
        if (err) {
            Respuesta(res, 500, 'Error al realizar la petición.' + err, null);
        } else {
            Respuesta(res, 200, '', tareasStored);
        }
    });
}

//Funcion para retornar la informacion
function Respuesta(res, estatus, mensaje, respuesta) {
    let objRespuesta = {
        Estatus: estatus,
        Mensaje: mensaje,
        Respuesta: respuesta
    }
    return res.status(estatus).send(objRespuesta);
}

//Metodos del controlador
module.exports = {
    Guardar,
    Consultar
}