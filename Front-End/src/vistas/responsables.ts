import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { EnumVistas } from 'enumeradores/enum-vistas';
 import * as eventos from 'eventos/eventos';
import { Responsable } from '../modelos/responsable';
import { ApiRespuesta } from "../servicios/respuesta-api";
import { EnumRespuestaAPI } from "../enumeradores/enum-respuesta-api";
import { ApiWebSocket } from '../servicios/webapi/api-websocket';
import { Sesion } from "../modelos/sesion";
import { DtoResponsables } from "../dto/dtoresponsables";

@autoinject
export class Responsables{    
    constructor(private ea: EventAggregator, private apiRespuesta: ApiRespuesta, private responsable: Responsable, private sesion: Sesion, private apiWebSocket: ApiWebSocket){
        responsable.Responsable= new DtoResponsables();
    }

    Guardar(){
        this.responsable.Guardar()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                if(this.sesion.Nombre == ""){
                    this.sesion.Nombre = this.responsable.Responsable.Nombre + " " + this.responsable.Responsable.Apellido;
                    this.sesion.Correo = this.responsable.Responsable.Correo;
                    this.apiWebSocket.conectar();
                    this.ea.publish(new eventos.VistaTrabajo(EnumVistas.vistaDashboard));
                    setTimeout(() => {
                        this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaTareas));
                    }, 400);
                }
            }
        });
    }
}