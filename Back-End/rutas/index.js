'use strict'

const express = require('express');
const categoriaCtrl = require('../controladores/categorias');
const responsablesCtrl = require('../controladores/responsables');
const tareasCtrl = require('../controladores/tareas');
const api = express.Router();


//Responsables
api.post('/responsables', responsablesCtrl.Guardar);
api.get('/responsables', responsablesCtrl.Consultar);
api.get('/responsables/id/:responsableid', responsablesCtrl.ConsultarId);
api.put('/responsables/correo/:correo', responsablesCtrl.Iniciar);

//Tareas
api.post('/tareas', tareasCtrl.Guardar);
api.get('/tareas', tareasCtrl.Consultar);

//Categorias
api.post('/categorias', categoriaCtrl.Guardar);
api.get('/categorias', categoriaCtrl.Consultar);

module.exports = api;