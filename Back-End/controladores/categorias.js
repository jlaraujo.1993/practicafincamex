'use strict'

const Categorias = require('../modelos/categorias');

//Guardar Categorias
function Guardar(req, res) {
    if (req.body._id == "") {
        let categoria = new Categorias();
        categoria.Descripcion = req.body.Descripcion;
        categoria.save((err, categoriaStored) => {
            if (err) {
                Respuesta(res, 500, 'Error al guardar en la base de datos.' + err, null);
            }
            Respuesta(res, 200, 'Categoria registrado con éxito.', categoriaStored);
        });
    } else {
        Categorias.update({ _id: req.body._id }, { "$set": { Descripcion: req.body.Descripcion } }, function(err, doc) {
            if (err) {
                Respuesta(res, 400, 'Error' + err, null);
            } else {
                Respuesta(res, 200, 'Categoria actualizada con éxito', null);
            }
        });

    }

}

//Consultar Responsables
function Consultar(req, res) {
    Categorias.find({}, (err, categoriasStored) => {
        if (err) {
            Respuesta(res, 500, 'Error al realizar la petición.' + err, null);
        } else {
            Respuesta(res, 200, '', categoriasStored);
        }
    });
}



//Funcion para retornar la informacion
function Respuesta(res, estatus, mensaje, respuesta) {
    let objRespuesta = {
        Estatus: estatus,
        Mensaje: mensaje,
        Respuesta: respuesta
    }
    return res.status(estatus).send(objRespuesta);
}

//Metodos del controlador
module.exports = {
    Guardar,
    Consultar
}