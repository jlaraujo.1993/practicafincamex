module.exports = {
    port: process.env.PORT || 9001,
    db: process.env.MONGODB || 'mongodb://localhost:27017/Fincamex',
    portWebSocket: process.env.PORTWEBSOCKET || "1337"
}