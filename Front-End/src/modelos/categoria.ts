import { autoinject } from 'aurelia-framework'; 
import { EnumRespuestaAPI } from 'enumeradores/enum-respuesta-api'; 
import { RespuestaApi } from 'servicios/respuesta-api'; 
import { DtoCategorias } from '../dto/dtocategorias';
import { ApiCategoria } from 'servicios/webapi/api-categoria';

@autoinject
export class Categoria {
    Categoria: DtoCategorias;
    ListadoCategorias: DtoCategorias[] =  []
   
    Respuesta: RespuestaApi = new RespuestaApi();
    constructor(private peticion: ApiCategoria){ 
        this.Categoria = new DtoCategorias();
        this.Consultar();
    }
    Consultar(){
        this.peticion.Consultar()
        .then(res => {  
            this.ListadoCategorias = res.Respuesta;
        }); 
    }
 
    Guardar(){ 
        if (this.Categoria.Descripcion.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "La Descripción es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else  {
            return new Promise<any>(result => {
                this.peticion.Guardar(this.Categoria)
                .then(res => {
                    return result(res);  
                });                
            }); 
        } 
    }
}