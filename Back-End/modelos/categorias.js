const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Entidad de Categorias
const CategoriasSchema = Schema({
    Descripcion: String
});

module.exports = mongoose.model('Categorias', CategoriasSchema);