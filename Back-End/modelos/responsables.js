const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Entidad de Responsables
const ResponsablesSchema = Schema({
    Nombre: String,
    Apellido: String,
    Correo: String,
    Contrasena: String
});

module.exports = mongoose.model('Responsables', ResponsablesSchema);