import { autoinject, observable } from 'aurelia-framework';  
import { EnumRespuestaAPI } from 'enumeradores/enum-respuesta-api'; 
import { RespuestaApi } from 'servicios/respuesta-api';
import { DtoSubTareas, DtoTareas } from "../dto/dtotareas";
import { ApiTarea } from 'servicios/webapi/api-tarea';

@autoinject
export class Tarea {
    Tarea: DtoTareas;


    Respuesta: RespuestaApi = new RespuestaApi();
    SubTarea: string = "";
    LisSubTareas: DtoSubTareas[] = [];



    //Filtro de Tareas
    @observable FiltroResponsable: any;
    @observable FiltroCategoria: any;
    @observable FiltroFechaInicio: any;
    @observable FiltroFechaFin: any;
    LisTareas: DtoTareas[]=[];
    LisTareasMostrar: DtoTareas[]=[];
    constructor(private peticion: ApiTarea){ 
       this.Tarea = new DtoTareas();
    }

    FiltroFechaInicioChanged() { 
        this.LisTareasMostrar =  this.LisTareas.filter(m => {
            if(m.FechaInicio >= this.FiltroFechaInicio && (this.FiltroFechaFin == undefined ? 1 == 1 : m.FechaFin <= this.FiltroFechaFin) && (this.FiltroCategoria == 0 ? 1 == 1 : m.Categoria == this.FiltroCategoria) && (this.FiltroResponsable == 0 ? 1 == 1 : m.Responsable == this.FiltroResponsable))
                return m;
        });  
    }

    FiltroFechaFinChanged() { 
        this.LisTareasMostrar =  this.LisTareas.filter(m => {
            if(m.FechaFin <= this.FiltroFechaFin && (this.FiltroFechaInicio == undefined ? 1 == 1 : m.FechaInicio >= this.FiltroFechaInicio)  && (this.FiltroCategoria == 0 ? 1 == 1 : m.Categoria == this.FiltroCategoria) && (this.FiltroResponsable == 0 ? 1 == 1 : m.Responsable == this.FiltroResponsable))
                return m;
        });  
    }

    FiltroResponsableChanged() {
        if(this.FiltroResponsable != 0){
            this.LisTareasMostrar =  this.LisTareas.filter(m => {
                if(m.Responsable == this.FiltroResponsable && (this.FiltroFechaFin == undefined ? 1 == 1 : m.FechaFin <= this.FiltroFechaFin)  && (this.FiltroFechaInicio == undefined ? 1 == 1 : m.FechaInicio >= this.FiltroFechaInicio) && (this.FiltroCategoria == 0 ? 1 == 1 : m.Categoria == this.FiltroCategoria))
                    return m;
            }); 
        } else {
            if(this.FiltroCategoria == 0){
                this.LisTareasMostrar =  this.LisTareas.filter(m => {
                    if((this.FiltroFechaFin == undefined ? 1 == 1 : m.FechaFin <= this.FiltroFechaFin)  && (this.FiltroFechaInicio == undefined ? 1 == 1 : m.FechaInicio >= this.FiltroFechaInicio))
                        return m;
                }); 
            } else {
                this.LisTareasMostrar =  this.LisTareas.filter(m => {
                    if(m.Categoria == this.FiltroCategoria && (this.FiltroFechaFin == undefined ? 1 == 1 : m.FechaFin <= this.FiltroFechaFin)  && (this.FiltroFechaInicio == undefined ? 1 == 1 : m.FechaInicio >= this.FiltroFechaInicio))
                        return m;
                }); 
            }
        }
    }

    FiltroCategoriaChanged() {
        if(this.FiltroCategoria != 0){
            this.LisTareasMostrar =  this.LisTareas.filter(m => {
                if(m.Categoria == this.FiltroCategoria && (this.FiltroFechaFin == undefined ? 1 == 1 : m.FechaFin <= this.FiltroFechaFin)  && (this.FiltroFechaInicio == undefined ? 1 == 1 : m.FechaInicio >= this.FiltroFechaInicio) && (this.FiltroResponsable == 0 ? 1 == 1 : m.Responsable == this.FiltroResponsable))
                    return m;
            }); 
        } else {
            if(this.FiltroResponsable == 0){
                this.LisTareasMostrar =  this.LisTareas.filter(m => {
                    if((this.FiltroFechaFin == undefined ? 1 == 1 : m.FechaFin <= this.FiltroFechaFin)  && (this.FiltroFechaInicio == undefined ? 1 == 1 : m.FechaInicio >= this.FiltroFechaInicio))
                        return m;
                }); 
            } else {
                this.LisTareasMostrar =  this.LisTareas.filter(m => {
                    if(m.Responsable == this.FiltroResponsable && (this.FiltroFechaFin == undefined ? 1 == 1 : m.FechaFin <= this.FiltroFechaFin)  && (this.FiltroFechaInicio == undefined ? 1 == 1 : m.FechaInicio >= this.FiltroFechaInicio))
                        return m;
                }); 
            } 
        }
    }

    ConsultarTareas(){
        this.peticion.Consultar()
        .then(res => { 
            this.LisTareas = res.Respuesta;
            this.LisTareasMostrar = this.LisTareas;
        });  
    }
 
    Guardar(){
        if (this.Tarea.Titulo.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "El Titulo es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Tarea.Responsable == 0) {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "El Responsable es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        }
        else if (this.Tarea.FechaInicio == undefined) {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "La Fecha Inicio es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Tarea.FechaFin == undefined) {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "El Fecha Fin es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Tarea.PorcentajeAvance.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "El Porcentaje de Avance es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Tarea.Categoria == 0) {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "La Categoria es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } 
        else {
            this.Tarea.ListadoSubTareas = this.LisSubTareas;
            return new Promise<any>(result => {
                this.peticion.Guardar(this.Tarea)
                .then(res => {
                    this.LisSubTareas = [];
                    this.Tarea = new DtoTareas();
                    return result(res);  
                });                
            }); 
        }
    }

    AgregarSubTarea(){
        if (this.SubTarea.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "La descripción de la subtarea es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else {
            let sub = new DtoSubTareas();
            sub.Descripcion = this.SubTarea;
            this.LisSubTareas.push(sub);
            this.SubTarea = "";
            this.Respuesta.Estatus = EnumRespuestaAPI.Aceptado;
            this.Respuesta.Mensaje = "";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            });            
        }
    } 
    
    QuitarSubTarea(item: DtoSubTareas){
        this.LisSubTareas = this.LisSubTareas.filter(s => {
            if(s.Descripcion != item.Descripcion)                
                return s;            
        })
    }
}