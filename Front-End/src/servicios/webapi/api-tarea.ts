import { autoinject } from "aurelia-framework";
import environment from 'environment';
import { ApiPeticion } from '../api-peticion';
import { DtoTareas } from '../../dto/dtotareas';

class ApiTareaMethods {
    constructor(private apiBase: string) {
        this.apiBase = this.apiBase + "{0}";
    };

    Guardar() {
        return this.apiBase["format"]('');
    };

    Consultar() {
        return this.apiBase["format"]('');
    };  
}

@autoinject
export class ApiTarea {
    apis: ApiTareaMethods;

    constructor(private api: ApiPeticion) {
        this.apis = new ApiTareaMethods(environment.apiUrl.tareas);
    }
     

    public Guardar(tarea: DtoTareas){
        return new Promise<any>(result => {
            this.api.post(this.apis.Guardar(), tarea)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    public Consultar(){
        return new Promise<any>(result => {
            this.api.get(this.apis.Consultar())
            .then(respuesta => {
                return result(respuesta);
            });
        });
    } 
}
