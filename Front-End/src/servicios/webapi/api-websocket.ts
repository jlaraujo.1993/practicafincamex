import { autoinject } from "aurelia-framework";
import { EventAggregator } from 'aurelia-event-aggregator';
import environment from 'environment';
import { Sesion } from '../../modelos/sesion';
import * as eventos from '../../eventos/eventos';

@autoinject
export class ApiWebSocket {
    socket: WebSocket;

    constructor(private ea: EventAggregator, private sesion: Sesion) {

    }

    private onMessage(ev: MessageEvent){
        let obj = JSON.parse(ev.data);
        this.ea.publish(new eventos.MostrarMensaje('positive', obj.Mensaje)); 
    }

    EnviarMensaje(obj: any){
        if (this.socket && this.socket.readyState == this.socket.OPEN) { 
            this.socket.send(JSON.stringify(obj));
        } else if (this.socket && this.socket.readyState == this.socket.CLOSED) {
            this.conectar();
        }
    }

    conectar(){ 
        this.socket = new WebSocket(environment.socket.servicioSokect+this.sesion.Correo);
        var self = this;
        this.socket.onmessage = (ev: MessageEvent) => {
           this.onMessage(ev);
         };
    }

    cerrar(Correo) {
        if (this.socket && this.socket.readyState == this.socket.OPEN){
            let obj = {NombreUsuario: Correo, Key: ''};
            this.socket.close(1000,Correo); 
        }
    }
}
