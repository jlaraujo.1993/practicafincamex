export class DtoTareas {
  _id: string;
  Titulo: string;
  Responsable: any;
  FechaInicio: Date;
  FechaFin: Date;
  PorcentajeAvance: string;
  Categoria: any;
  ListadoSubTareas: any[];
  Visible: boolean;
    constructor(){
      this._id="";
      this.Titulo="";
      this.Responsable="";
      this.PorcentajeAvance="";
      this.Categoria="";
      this.ListadoSubTareas= [];
      this.Visible = true;
    }  
} 

export class DtoSubTareas {
    Descripcion: string; 
    constructor(){
      this.Descripcion = ''; 
    }  
} 