 export default {
  debug: true,
  testing: true,
  apiUrl: {
    responsbales: 'http://localhost:9001/api/responsables',
    categorias: 'http://localhost:9001/api/categorias',
    tareas: 'http://localhost:9001/api/tareas'
  },
  socket: {
    servicioSokect: 'ws://localhost:1337/'
  }
};
