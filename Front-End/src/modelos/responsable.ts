import { autoinject } from 'aurelia-framework'; 
import { EnumRespuestaAPI } from 'enumeradores/enum-respuesta-api'; 
import { RespuestaApi } from 'servicios/respuesta-api'; 
import { DtoResponsables } from '../dto/dtoresponsables';
import { ApiResponsable } from 'servicios/webapi/api-responsables';

@autoinject
export class Responsable {
    Responsable: DtoResponsables;
    ListadoResponsable: DtoResponsables[] =  []
    Respuesta: RespuestaApi = new RespuestaApi();

    constructor(private peticion: ApiResponsable){ 
        this.Responsable = new DtoResponsables();
        this.Consultar();
    }
    Consultar(){
        this.peticion.Consultar()
        .then(res => { 
            this.ListadoResponsable = res.Respuesta;
        }); 
    }

    Iniciar(){
        if (this.Responsable.Correo.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "El Correo o nombre de usuario es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Responsable.Contrasena.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "La Contraseña es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else {
            return new Promise<any>(result => {
                let obj ={
                    Contrasena: this.Responsable.Contrasena
                };
                this.peticion.Iniciar(this.Responsable.Correo, obj)
                .then(res => { 
                    return result(res);
                });             
            });
        }
    }

    Guardar(){ 
        if (this.Responsable.Nombre.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "El Nombre es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Responsable.Apellido.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "El Apellido es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Responsable.Correo.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "El Correo es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Responsable.Contrasena.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "La Contraseña es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Responsable.Contrasena.trim() != this.Responsable.RepContrasena.trim()) {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "Las Contraseñas no coinciden.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        }  else  {
            return new Promise<any>(result => {
                this.peticion.Guardar(this.Responsable)
                .then(res => {
                    return result(res);  
                });                
            }); 
        } 
    }
}