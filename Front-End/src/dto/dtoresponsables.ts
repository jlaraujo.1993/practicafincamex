
export class DtoResponsables {
  _id: string;
  Nombre: string;
  Apellido: string;
  Correo: string;
  Contrasena: string;
  RepContrasena: string;
  constructor(){
    this._id = '';
    this.Nombre = '';
    this.Apellido = '';
    this.Correo = '';
    this.Contrasena = ''; 
    this.RepContrasena = ''; 
  }  
} 