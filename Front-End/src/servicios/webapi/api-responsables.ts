import { autoinject } from "aurelia-framework";
import environment from 'environment';
import { ApiPeticion } from '../api-peticion';
import { DtoResponsables } from '../../dto/dtoresponsables';

class ApiResponsablesMethods {
    constructor(private apiBase: string) {
        this.apiBase = this.apiBase + "{0}";
    };
    Guardar() {
        return this.apiBase["format"]('');
    };
    Consultar() {
        return this.apiBase["format"]('');
    };
   
    Iniciar(correo: string) {
        return this.apiBase["format"]('/correo/'+correo);
    };
       
}

@autoinject
export class ApiResponsable {
    apis: ApiResponsablesMethods;

    constructor(private api: ApiPeticion) {
        this.apis = new ApiResponsablesMethods(environment.apiUrl.responsbales);
    }
     
    public Consultar(){
        return new Promise<any>(result => {
            this.api.get(this.apis.Consultar())
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    public Iniciar(correo: string, obj: any){
        return new Promise<any>(result => {
            this.api.put(this.apis.Iniciar(correo),obj)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    public Guardar(responsable: DtoResponsables){
        return new Promise<any>(result => {
            this.api.post(this.apis.Guardar(), responsable)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
}
