import { EnumVistas } from 'enumeradores/enum-vistas';
export class Tareas {
    
    VistasTarea: string;
    VistasModelosTareas: string;
    constructor(){
        this.VistasTarea = EnumVistas.vistaListadoTareas["vista"];
        this.VistasModelosTareas = EnumVistas.vistaListadoTareas["modelo"];
    }

    AgregarTarea(){ 
        this.VistasTarea = EnumVistas.vistaAgregarTareas["vista"];
        this.VistasModelosTareas = EnumVistas.vistaAgregarTareas["modelo"];
    }

    Listado(){
        this.VistasTarea = EnumVistas.vistaListadoTareas["vista"];
        this.VistasModelosTareas = EnumVistas.vistaListadoTareas["modelo"];
    }  
}