const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Entidad de Tareas
const TareasSchema = Schema({
    Titulo: String,
    Responsable: String,
    FechaInicio: String,
    FechaFin: String,
    PorcentajeAvance: String,
    Categoria: String,
    ListadoSubTareas: []
});

module.exports = mongoose.model('Tareas', TareasSchema);