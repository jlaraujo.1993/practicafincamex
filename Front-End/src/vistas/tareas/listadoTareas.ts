import { autoinject } from 'aurelia-framework';
import { Responsable } from '../../modelos/responsable';
import { Categoria } from '../../modelos/categoria';
import { Tarea } from '../../modelos/tarea';

@autoinject
export class ListadoTareas {
    constructor(private categoria: Categoria, private responsable: Responsable, private tarea: Tarea){
        tarea.ConsultarTareas();
        responsable.Consultar();
        categoria.Consultar();
    }
}