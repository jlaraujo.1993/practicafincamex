'use strict'

const Responsables = require('../modelos/responsables');

//Guardar Responsables
function Guardar(req, res) {
    Responsables.find({ Correo: req.body.Correo }, function(err, responsableStored) {
        if (err) {
            Respuesta(res, 500, 'Error al realizar la petición.' + err, null);
        } else if (responsableStored.length == 0) {
            let responsables = new Responsables();
            responsables.Nombre = req.body.Nombre;
            responsables.Apellido = req.body.Apellido;
            responsables.Correo = req.body.Correo;
            responsables.Contrasena = req.body.Contrasena;
            responsables.save((err, pagoStored) => {
                if (err) {
                    Respuesta(res, 500, 'Error al guardar en la base de datos.' + err, null);
                }
                Respuesta(res, 200, 'Responsable registrado con éxito.', pagoStored);
            });
        } else {
            Respuesta(res, 503, 'Este Correo eletrónico o Nombre de usuario ya éxisten.', null);
        }
    });
}

//Consultar Responsables
function Consultar(req, res) {
    Responsables.find({}, (err, responsablesStored) => {
        if (err) {
            Respuesta(res, 500, 'Error al realizar la petición.' + err, null);
        } else {
            Respuesta(res, 200, '', responsablesStored);
        }
    });
}

//Consultar por Id el Responsabe
function ConsultarId(req, res) {
    Responsables.findById(req.params.responsableid, (err, responsableStored) => {
        if (err) {
            Respuesta(res, 400, 'Error' + err, null);
        } else if (!responsableStored) {
            Respuesta(res, 404, 'No existen Responsables.', null);
        } else {
            Respuesta(res, 200, '', responsableStored);
        }
    });
}

//Iniciar
function Iniciar(req, res) {
    Responsables.find({ Correo: req.params.correo, Contrasena: req.body.Contrasena }, function(err, responsableStored) {
        if (err) {
            Respuesta(res, 500, 'Error al realizar la petición.' + err, null);
        } else if (responsableStored.length == 0) {
            Respuesta(res, 404, 'Datos Incorrectos.', null);
        } else {
            Respuesta(res, 200, 'Bienvenido', responsableStored);
        }
    });
}


//Funcion para retornar la informacion
function Respuesta(res, estatus, mensaje, respuesta) {
    let objRespuesta = {
        Estatus: estatus,
        Mensaje: mensaje,
        Respuesta: respuesta
    }
    return res.status(estatus).send(objRespuesta);
}

//Metodos del controlador
module.exports = {
    Guardar,
    Consultar,
    ConsultarId,
    Iniciar
}