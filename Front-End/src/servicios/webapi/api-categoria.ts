import { autoinject } from "aurelia-framework";
import environment from 'environment';
import { ApiPeticion } from '../api-peticion';
import { DtoCategorias } from '../../dto/dtocategorias';

class ApiCategoriasMethods {
    constructor(private apiBase: string) {
        this.apiBase = this.apiBase + "{0}";
    };

    Guardar() {
        return this.apiBase["format"]('');
    };

    Consultar() {
        return this.apiBase["format"]('');
    };  
}

@autoinject
export class ApiCategoria {
    apis: ApiCategoriasMethods;

    constructor(private api: ApiPeticion) {
        this.apis = new ApiCategoriasMethods(environment.apiUrl.categorias);
    }
     

    public Guardar(categoria: DtoCategorias){
        return new Promise<any>(result => {
            this.api.post(this.apis.Guardar(), categoria)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    public Consultar(){
        return new Promise<any>(result => {
            this.api.get(this.apis.Consultar())
            .then(respuesta => {
                return result(respuesta);
            });
        });
    } 
}
