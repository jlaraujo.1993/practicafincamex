import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { EnumVistas } from 'enumeradores/enum-vistas';
import * as eventos from 'eventos/eventos';
import { Responsable } from '../modelos/responsable';
import { ApiRespuesta } from "../servicios/respuesta-api";
import { EnumRespuestaAPI } from "../enumeradores/enum-respuesta-api";
import { ApiWebSocket } from '../servicios/webapi/api-websocket';
import { Sesion } from 'modelos/sesion';


@autoinject
export class Login {
    constructor(private ea: EventAggregator, private apiRespuesta: ApiRespuesta, private responsable: Responsable, private sesion: Sesion, private apiWebSocket: ApiWebSocket){

    }
    
    Iniciar(){
        this.responsable.Iniciar()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                this.sesion.Nombre = respuesta.Respuesta[0].Nombre + " " + respuesta.Respuesta[0].Apellido;
                this.sesion.Correo = respuesta.Respuesta[0].Correo;
                this.apiWebSocket.conectar();
                this.ea.publish(new eventos.VistaTrabajo(EnumVistas.vistaDashboard));
                setTimeout(() => {
                    this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaTareas));
                }, 400);
            }
        });
    }
    
    Registrarse(){
        this.ea.publish(new eventos.VistaTrabajo(EnumVistas.vistaResponsables));
    }
}
