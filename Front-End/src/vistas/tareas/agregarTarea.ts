import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Responsable } from '../../modelos/responsable';
import { Categoria } from '../../modelos/categoria';
import { Tarea } from '../../modelos/tarea';

import { ApiRespuesta } from "../../servicios/respuesta-api";
import { EnumRespuestaAPI } from "../../enumeradores/enum-respuesta-api";
import { ApiWebSocket } from '../../servicios/webapi/api-websocket';
import { Sesion } from 'modelos/sesion';


@autoinject
export class AgregarTareas { 
    constructor(private ea: EventAggregator, private apiRespuesta: ApiRespuesta, private categoria: Categoria, private responsable: Responsable, private tarea: Tarea, private sesion: Sesion, private apiWebSocket: ApiWebSocket){
        
    }

    AgregarSubTarea(){
        this.tarea.AgregarSubTarea()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
            }
        });
    }

    Guardar(){
        this.tarea.Guardar()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){                
                let wsobj = {
                    NombreUsuario: respuesta.Respuesta.Responsable,
                    Mensaje: this.sesion.Nombre + " te asigno la tarea '" + respuesta.Respuesta.Titulo + "', con vigencia asta " + respuesta.Respuesta.FechaFin
                }; 
                this.apiWebSocket.EnviarMensaje(wsobj);               
            }
        });
    }
}