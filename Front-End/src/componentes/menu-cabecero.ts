import { autoinject } from 'aurelia-framework'; 
import { EventAggregator } from 'aurelia-event-aggregator'; 
import * as eventos from '../eventos/eventos'; 
import { EnumVistas } from 'enumeradores/enum-vistas';  
import { Sesion } from 'modelos/sesion'; 
import { ApiWebSocket } from '../servicios/webapi/api-websocket';
import { Responsable } from '../modelos/responsable';

@autoinject
export class MenuCabecero {
    
    constructor(private ea: EventAggregator, private sesion: Sesion, private responsable: Responsable, private apiWebSocket: ApiWebSocket ){
    
    }
    //Mostrar pantalla de Tareas
    AgregarTareas(){
        this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaTareas));
    }
    //Mostrar pantalla de Categoria
    AgregarCategorias(){
        this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaCategorias));
    }
    //Mostrar pantalla de Responsables
    AgregarResponsables(){
        this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaDsResponsables));
    }

    //Cerrar sesion
    CerrarSesion(){      
        this.apiWebSocket.cerrar(this.sesion.Correo);                       
        location.reload(true);
    } 
}