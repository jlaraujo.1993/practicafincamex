export enum EnumVistas { 
    vistaNinguna = <any>{vista: '', modelo: ''},
    vistaLogin = <any>{vista: './vistas/login.html', modelo: './vistas/login'},
    vistaResponsables = <any>{vista: './vistas/responsables.html', modelo: './vistas/responsables'},
    vistaDsResponsables = <any>{vista: './responsables.html', modelo: './responsables'},
    vistaDashboard = <any>{vista: './vistas/dashboard.html', modelo: './vistas/dashboard'},
    vistaTareas = <any>{vista: './tareas/tareas.html', modelo: './tareas/tareas'},
    vistaAgregarTareas = <any>{vista: './agregarTarea.html', modelo: './agregarTarea'},
    vistaListadoTareas = <any>{vista: './listadoTareas.html', modelo: './listadoTareas'},
    vistaCategorias = <any>{vista: './categorias.html', modelo: './categorias'}
} 
