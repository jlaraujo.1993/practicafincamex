
import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Categoria } from '../modelos/categoria';
import { ApiRespuesta } from "../servicios/respuesta-api";
import { EnumRespuestaAPI } from "../enumeradores/enum-respuesta-api";

@autoinject
export class Categorias{
    
    constructor(private ea: EventAggregator, private apiRespuesta: ApiRespuesta, private categoria: Categoria){

    }

    Guardar(){
        this.categoria.Guardar()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                this.categoria.Categoria.Descripcion ="";
                this.categoria.Categoria._id = "";
            }
        });
    }
}